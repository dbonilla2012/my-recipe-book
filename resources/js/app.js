// Vue routes
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './App.vue'
import MainPage from './components/MainPage'
import Recipes from './components/Recipes'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: App
        },
        {
            path: '/home',
            name: 'MainPage',
            component: MainPage,
        },
        {
            path: '/recipes',
            name: 'Recipes',
            component: Recipes,
        },
    ],
});


require('./bootstrap');

window.Vue = require('vue');


Vue.component('app', require('./App.vue').default);

const app = new Vue({
    el: '#app',
    router,
});
